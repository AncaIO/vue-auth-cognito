const prefix = 'vue-auth-cognito'

export const AUTHENTICATE = `${prefix}/AUTHENTICATE`
export const NEW_PASSWORD_REQUIRED = `${prefix}/NEW_PASSWORD_REQUIRED`
export const SET_USER = `${prefix}/SET_USER`
export const USER_CONFIRMATION_NECESSARY = `${prefix}/USER_CONFIRMATION_NECESSARY`
export const ATTRIBUTES = `${prefix}/ATTRIBUTES`
export const SIGNOUT = `${prefix}/SIGNOUT`

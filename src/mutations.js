import * as types from './mutation-types';

export default {
  [types.AUTHENTICATE] (state, payload) {
    state.user = payload
  },
  [types.NEW_PASSWORD_REQUIRED] (state, payload) {
    state.newPasswordRequired = payload
  },
  [types.USER_CONFIRMATION_NECESSARY] (state, payload) {
    state.userConfirmationNecessary = payload
  },
  [types.SET_USER] (state, payload) {
    state.cognitoUser = payload
  },
  [types.SIGNOUT] (state) {
    state.user = null
  },
  [types.ATTRIBUTES] (state, payload) {
    state.user.attributes = payload
  }
};

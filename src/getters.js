export default {
  user: state => {
    return state.user
  },
  newPasswordRequired: state => {
    return state.newPasswordRequired
  },
  cognitoUser: state => {
    return state.cognitoUser
  }
}
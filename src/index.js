import ActionsFactory from './actions'
import mutations from './mutations'
import getters from './getters'
const state = {
  user: null,
  newPasswordRequired: false,
  userConfirmationNecessary: false,
  cognitoUser: null
}

export default class CognitoAuth {
  constructor (config) {
    this.state = state
    this.actions = new ActionsFactory(config)
    this.mutations = mutations
    this.getters = getters
  }
}
